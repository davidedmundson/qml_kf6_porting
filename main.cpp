// Copyright (C) 2019 The Qt Company Ltd.
// Copyright (C) 2023 David Edmundson <davidedmundson@kde.org>
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QCommandLineParser>

#include "kf6convert.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("qmlformat");
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);

    QCommandLineParser parser;
    QCommandLineOption inPlaceOption({"i","inplace"}, "Edit files in place");
    QCommandLineOption formatOnlyOption({"f","onlyformat"}, "Format files only");


    parser.addOption(inPlaceOption);
    parser.addOption(formatOnlyOption);


    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(app);


    bool success = true;
    for (const QString &file : parser.positionalArguments()) {

        if (parser.isSet(formatOnlyOption)) {
            qWarning() << "parsing file" << file;
            QmlParser converter(file);
            converter.setInPlace(parser.isSet(inPlaceOption));
            converter.parse();
            continue;
        }
        KF6Convert converter(file);
        converter.setInPlace(parser.isSet(inPlaceOption));
        converter.parse();

    }
    return success ? 0 : 1;
}
