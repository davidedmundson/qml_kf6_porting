#include "kf6convert.h"

#include <QtQml/private/qqmljslexer_p.h>
#include <QtQml/private/qqmljsparser_p.h>
#include <QtQml/private/qqmljsengine_p.h>
#include <QtQml/private/qqmljsastvisitor_p.h>
#include <QtQml/private/qqmljsast_p.h>
#include <QtQmlDom/private/qqmldomitem_p.h>
#include <QtQmlDom/private/qqmldomexternalitems_p.h>
#include <QtQmlDom/private/qqmldomtop_p.h>
#include <QtQmlDom/private/qqmldomoutwriter_p.h>

#include <QDebug>

/**
 * To extend this converter create a method using the template in the file
 *
 * For every DOM element this method will be visited.
 * Create a MutableDomItem from the item, make any modifications and call commitToBase
 * Then add your function to the parseItem callback at the end
 */

using namespace QQmlJS::Dom;

void KF6Convert::portGraphicalEffects(DomItem &item) {
    if (item.internalKind() == DomType::Import) {
        MutableDomItem mutableItem(item);
        Import *import = mutableItem.mutableAs<Import>();

        if (import->uri.moduleUri() == "QtGraphicalEffects") {
            import->uri = QmlUri::fromUriString("Qt5Compat.GraphicalEffects");
            import->version = Version(6, 0);
            mutableItem.commitToBase();
        }
    }

    if (item.internalKind() == DomType::QmlObject) {
        if (fullModuleName(item.name()) == "QtGraphicalEffects.DropShadow" ||
            fullModuleName(item.name()) == "QtGraphicalEffects.GaussianBlur") {
            MutableDomItem mutableItem(item);
            QmlObject *dropShadow = mutableItem.mutableAs<QmlObject>();
            auto bindings = dropShadow->bindings();
            bindings.remove("samples");
            dropShadow->setBindings(bindings);
            mutableItem.commitToBase();
        }
    }
}

void KF6Convert::portDialogs(DomItem &item) {
    if (item.internalKind() == DomType::Import) {
        MutableDomItem mutableItem(item);
        Import *import = mutableItem.mutableAs<Import>();

        if (import->uri.moduleUri() == "QtQuick.Dialogs") {
            import->uri = QmlUri::fromUriString("QtQuick.Dialogs");
            import->version = Version(6, 3);
            mutableItem.commitToBase();
        }
    }
    if (fullModuleName(item.name()) == "QtQuick.Dialogs.FileDialog") {
        MutableDomItem mutableItem(item);
        QmlObject *dropShadow = mutableItem.mutableAs<QmlObject>();
        auto bindings = dropShadow->bindings();
        bindings.remove("selectExisting");
        bindings.remove("selectMultiple");
        bindings.remove("selectFolder");
        dropShadow->setBindings(bindings);
        mutableItem.commitToBase();
    }
}

void KF6Convert::portBasicListItemIcon(DomItem &item) {
    if (fullModuleName(item.name()) == "org.kde.kirigami.BasicListItem") {
        MutableDomItem mutableItem(item);
        QmlObject *dropShadow = mutableItem.mutableAs<QmlObject>();
        auto bindings = dropShadow->bindings();
        if (bindings.contains("icon")) {
            auto oldIconBinding = bindings.take("icon");
            if (oldIconBinding.scriptExpressionValue()) {
                auto newBinding = Binding("icon.name",  oldIconBinding.scriptExpressionValue()->code().toString(), oldIconBinding.bindingType());
                bindings.insert("icon", newBinding);
            }
        }
        dropShadow->setBindings(bindings);
        mutableItem.commitToBase();
    }
    if (fullModuleName(item.name()) == "org.kde.kirigami.Action") {
        MutableDomItem mutableItem(item);
        QmlObject *dropShadow = mutableItem.mutableAs<QmlObject>();
        auto bindings = dropShadow->bindings();
        if (bindings.contains("iconName")) {
            auto oldIconBinding = bindings.take("iconName");
            if (oldIconBinding.scriptExpressionValue()) {
                auto newBinding = Binding("icon.name",  oldIconBinding.scriptExpressionValue()->code().toString(), oldIconBinding.bindingType());
                bindings.insert("iconName", newBinding);
            }
        }
        dropShadow->setBindings(bindings);
        mutableItem.commitToBase();
    }
}


void KF6Convert::parseItem(DomItem &item)
{
    portGraphicalEffects(item);
    portDialogs(item);
    portBasicListItemIcon(item);
}
