#pragma once

// typedef void ConversionFunction(DomItem &item);

#include <QObject>
#include <QHash>

namespace QQmlJS {
    namespace Dom {
        class DomItem;
    }
}

class QmlParser{
public:
  QmlParser(const QString &fileName);
  bool parse();
  void setInPlace(bool inPlace) {
      m_inPlace = inPlace;
    }

  /**
   * Invoked for every item in the source tree
   */
  virtual void parseItem(QQmlJS::Dom::DomItem &item) {};

protected:
    /**
    * Assuming we have code in the form
    * import QtQuick.Controls 2.0 as MyAwesomeControls
    *
    * fullModuleName(MyAwesomeControls.TextField) returns  QtQuick.Controls.TextField
    */
    QString fullModuleName(const QString &name);

    /**
    * Assuming we have code in the form
    * import QtQuick.Controls 2.0 as MyAwesomeControls
    *
    * qualifiedName(QtQuick.Controls.TextField) returns MyAwesomeControls.TextField
    */
    QString qualifiedName(const QString &name);


private:
    QString m_filename;
    bool m_inPlace = false;
    QHash<QString, QString > m_namespaceToQualifier;
    QHash<QString, QString > m_qualifierToNamespace;
};
