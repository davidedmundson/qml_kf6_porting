#!/bin/sh

set -e

echo "Setting up:"
git fetch origin
git checkout -b work/d_ed/qml_port_temp
git reset --hard origin/master

echo "Formatting all QML"
find -name '*.qml' | xargs /home/david/projects/temp/qml_kf6_porting/build/kde6qmlconvert -i -f > /dev/null
git commit -a -m 'format QML'
formattingHash=`git rev-parse HEAD`

echo "Fixing porting issues"
find -name '*.qml' | xargs /home/david/projects/temp/qml_kf6_porting/build/kde6qmlconvert -i > /dev/null
git commit -a -m 'actual porting'

echo "Reverting formatting changes. Some manual fixes may be required"
git revert $formattingHash --no-edit

echo "Squashing all changes into work/d_ed/qml_port"
git checkout -b work/d_ed/qml_port
git reset --hard origin/master
git merge --squash work/d_ed/qml_port_temp
git commit -m 'Adapt to KF6'

