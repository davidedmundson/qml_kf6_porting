// Copyright (C) 2019 The Qt Company Ltd.
// Copyright (C) 2023 David Edmundson <davidedmundson@kde.org>
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

#include "qmlparser.h"

#include <QtQml/private/qqmljslexer_p.h>
#include <QtQml/private/qqmljsparser_p.h>
#include <QtQml/private/qqmljsengine_p.h>
#include <QtQml/private/qqmljsastvisitor_p.h>
#include <QtQml/private/qqmljsast_p.h>
#include <QtQmlDom/private/qqmldomitem_p.h>
#include <QtQmlDom/private/qqmldomexternalitems_p.h>
#include <QtQmlDom/private/qqmldomtop_p.h>
#include <QtQmlDom/private/qqmldomoutwriter_p.h>

#include <QCommandLineParser>

using namespace QQmlJS::Dom;

QmlParser::QmlParser(const QString &fileName)
    : m_filename(fileName)
{
}

bool QmlParser::parse()
{
    DomItem env =
            DomEnvironment::create(QStringList(),
                                   QQmlJS::Dom::DomEnvironment::Option::SingleThreaded
                                           | QQmlJS::Dom::DomEnvironment::Option::NoDependencies);

    DomItem tFile; // place where to store the loaded file
    env.loadFile(
            m_filename, QString(),
            [&tFile](Path, const DomItem &, const DomItem &newIt) {
                tFile = newIt; // callback called when everything is loaded that receives the loaded
                               // external file pair (path, oldValue, newValue)
            },
            LoadOption::DefaultLoad);
    env.loadPendingDependencies();
    DomItem qmlFile = tFile.fileObject();
    std::shared_ptr<QmlFile> qmlFilePtr = qmlFile.ownerAs<QmlFile>();
    if (!qmlFilePtr || !qmlFilePtr->isValid()) {
        qmlFile.iterateErrors(
                [](DomItem, ErrorMessage msg) {
                    errorToQDebug(msg);
                    return true;
                },
                true);
        qWarning().noquote() << "Failed to parse" << m_filename;
        return false;
    }


    // preprocess import IDs for the parser later
    // this is it's own loop as the top line of the file might not be the first item
    qmlFile.visitTree(Path(), [this](Path, DomItem &item, bool) -> bool {
        if (item.internalKind() == DomType::Import) {
            const Import *import = item.as<Import>();
            if (import->uri.isModule()) {
                QString moduleName = import->uri.moduleUri();
                QString importId = import->importId;
                if (!importId.isEmpty()) {
                    m_namespaceToQualifier[moduleName] = importId;
                    m_qualifierToNamespace[importId] = moduleName;
                }
            }
        }
        return true;
    });


    qmlFile.visitTree(Path(), [this](Path, DomItem &item, bool) -> bool {
        parseItem(item);
        return true;
    });


    // Turn AST back into source code

    LineWriterOptions lwOptions;
    lwOptions.updateOptions = LineWriterOptions::Update::None;
    lwOptions.attributesSequence = LineWriterOptions::AttributesSequence::Preserve;

    WriteOutChecks checks = WriteOutCheck::UpdatedDomStable;

    MutableDomItem res;
    if (m_inPlace) {
        qWarning().noquote() << "Writing to file" << m_filename;
        FileWriter fw;
        const unsigned numberOfBackupFiles = 0;
        res = qmlFile.writeOut(m_filename, numberOfBackupFiles, lwOptions, &fw, checks);
    } else {
        QFile out;
        out.open(stdout, QIODevice::WriteOnly);
        LineWriter lw([&out](QStringView s) { out.write(s.toUtf8()); }, m_filename, lwOptions);
        OutWriter ow(lw);
        res = qmlFile.writeOutForFile(ow, checks);
        ow.flush();
    }
    return bool(res);
}

QString QmlParser::fullModuleName(const QString &nameSpaceAndItemName)
{
    const int dotIndex = nameSpaceAndItemName.lastIndexOf('.');
    if (dotIndex < 0) {
        return nameSpaceAndItemName;
    }
    QString qualifier = nameSpaceAndItemName.left(dotIndex);
    const QString name = nameSpaceAndItemName.mid(dotIndex + 1);

    QString moduleName = qualifier;

    if (m_qualifierToNamespace.contains(qualifier)) {
        moduleName = m_qualifierToNamespace[qualifier];
    }
    return moduleName + "." + name;
}

QString QmlParser::qualifiedName(const QString &nameSpaceAndItemName)
{
    const int dotIndex = nameSpaceAndItemName.lastIndexOf('.');
    if (dotIndex < 0) {
        return nameSpaceAndItemName;
    }
    const QString nameSpace = nameSpaceAndItemName.left(dotIndex);
    const QString name = nameSpaceAndItemName.mid(dotIndex + 1);

    QString moduleName = nameSpace;
    if (m_namespaceToQualifier.contains(nameSpace)) {
        moduleName = m_namespaceToQualifier[nameSpace];
    }
    return moduleName + "." + name;
}
