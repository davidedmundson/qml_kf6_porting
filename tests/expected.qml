import Qt5Compat.GraphicalEffects 6.0 as MyGraphicalEffects
import QtQuick.Dialogs 6.3 as MyDialogs

Item {
    MyGraphicalEffects.DropShadow {
        cached: true
        radius: 6
    }
    MyGraphicalEffects.GaussianBlur {
        cached: true
        radius: 6
    }
    MyGraphicalEffects.SomethingElseThatShouldKeepSamples {
        cached: true
        radius: 6
        samples: 5
    }

    MyDialogs.FileDialog {
        otherRandomProp: false
    }
}
