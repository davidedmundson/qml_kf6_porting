import QtGraphicalEffects 1.0 as MyGraphicalEffects
import QtQuick.Dialogs 1.5 as MyDialogs

Item {
    MyGraphicalEffects.DropShadow {
        cached: true
        radius: 6
        samples: 5
    }
    MyGraphicalEffects.GaussianBlur {
        cached: true
        samples: 5
        radius: 6
    }
    MyGraphicalEffects.SomethingElseThatShouldKeepSamples {
        cached: true
        radius: 6
        samples: 5
    }

    MyDialogs.FileDialog {
        selectExisting: true
        selectMultiple: false
        selectFolder: true
        otherRandomProp: false
    }
}
