#pragma once

#include "qmlparser.h"

class KF6Convert : public QmlParser
{
public:
    KF6Convert(const QString &fileName) : QmlParser(fileName){}
protected:
    void parseItem(QQmlJS::Dom::DomItem &item) override;
private:
    void portGraphicalEffects(QQmlJS::Dom::DomItem &item);
    void portDialogs(QQmlJS::Dom::DomItem &item);
    void portBasicListItemIcon(QQmlJS::Dom::DomItem &item);
};
